
const bios = document.querySelectorAll("#nav-folder .bio");
const files = document.querySelectorAll("#main-folder .section-file");
const tabs = document.querySelectorAll("#main-folder .tab");
const folders = document.querySelectorAll("#main-folder .folder-content");

const p_buttons = document.querySelectorAll("#nav-folder .filtered-button");
const c_buttons = document.querySelectorAll("#nav-folder .content-button");

// give a folder and count the number of visible items in it
function fill_info(folder, partner = ""){
  i = 0;
  for (let file of folder.querySelectorAll(".section-file")){
    // if file is visible
    // if (file.offsetParent !== null){
    //   i = i + 1;
    // }
    if( ! file.classList.contains("hidden") ){
      i = i + 1;
    }
  }
  let d_number = folder.querySelector(".data-number");
  d_number.innerHTML = i;

  if (partner != ""){
    let d_partner = folder.querySelector(".data-partner");
    d_partner.innerHTML = "by " + partner;
  }
}

// only show the element that have a class 'select'
function filter(elements, select){
  elements.forEach((file) =>
    file.classList.contains(select)
      ? file.classList.remove("hidden")
      : file.classList.add("hidden")
  );
}

// when pressing a partner button
function filterPartners(e, partner) {
  
  filter(bios, partner);

  if (partner === "tekhne") {
    files.forEach((file) => file.classList.remove("hidden"));
  } else {
    filter(files, partner)
  }

  p_buttons.forEach((button) => {
    button.classList.remove("active");
  });
  e.target.classList.add("active");

  document.body.classList = partner;

  // update URL
  updateURLState('partner', partner);

  // count visible items
  for(let folder of folders){
    fill_info(folder, partner);
  }
}

// when pressing a content button
function filterMain(e, type) {

  console.log('call filter');

  // show only blog, calls, events
  filter(tabs, type);

  console.log('filtering done');

  // active button

  c_buttons.forEach((button) => {
    button.classList.remove("active");
  });
  document.getElementById(type + '-button').classList.add("active");

  console.log('button active');

  // update URL
  updateURLState('type', type);

  console.log('url updated');

  // count visible items
  for(let folder of folders){
    fill_info(folder);
  }
}

function seeAll(e){
  const url = new URL(location);
  url.searchParams.delete('partner');
  window.location.href = url;
}

function expandFileContent(e) {
  const fileContent = e.target.previousElementSibling;

  fileContent.classList.toggle("collapsed");

  e.target.innerText =
    e.target.innerText === "Show More" ? "Hide" : "Show More";
}

function updateURLState(key, value) {
  const url = new URL(location);
  url.searchParams.set(key, value);
  history.pushState({}, "", url);
}

function initializeURLState() {
  const url = new URL(location);
  const selectedPartner = url.searchParams.get("partner");
  const selectedType = url.searchParams.get("type");

  let partnerButton;
  if (selectedPartner) {
    partnerButton = document.querySelector(`#${selectedPartner}-button`);
  } else{
    // DEFAULT PARTNER
    partnerButton = document.querySelector(`#tekhne-button`);
  }

  let typeButton;
  if (selectedType) {
    typeButton = document.querySelector(`#${selectedType}-button`);
  } else{
    // DEFAULT PARTNER
    typeButton = document.querySelector(`#blogs-button`);
  }

  partnerButton.click();
  typeButton.click();
}

// --- MAIN IN VIEW

const main_folder = document.querySelector("#main-folder");
let is_main_in_view = false;
function main_in_view(){
  setTimeout(function(){

    if (window.scrollY > main_folder.offsetTop && ! is_main_in_view){
      main_folder.classList.add("in-view");
      is_main_in_view = true;
    } 
    else if (window.scrollY <= main_folder.offsetTop && is_main_in_view){
      main_folder.classList.remove("in-view");
      is_main_in_view = false;
    }
    
  }, 1000);
}

window.addEventListener("scroll", main_in_view);
main_in_view();
