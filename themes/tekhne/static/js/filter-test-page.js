
let logo_class = '.logo';
let button_class = '.filter-button .button-back';

let grid = document.querySelector("#test-grid");

function assign_filters(){

    jsFILTERS.forEach( (filter) => {

        // copy template for every filters
        let template = document.querySelector("#test-template");
        const clone = template.content.cloneNode(true);
        
        let logo = clone.querySelector(logo_class);
        let btns = clone.querySelectorAll(button_class); 
        logo.style.filter = "url(#" + filter.id + ")";
        btns.forEach((btn) => {
            btn.style.filter = "url(#" + filter.id + ")";
        })
        logo.classList.add('computed');

        let footer = clone.querySelector('.test-footer');
        footer.innerHTML =  '<p>'+filter.label+'</p>' + '<p>#'+filter.id+'</p>';

        let svgfilter = document.querySelector('#'+filter.id);
        let prims = Array.from(svgfilter.children);
        prims.forEach( (p) => {
            footer.innerHTML += '<p>'+ p.tagName +'</p>';
        });

        grid.appendChild(clone);
    });
}

assign_filters();


let primitives = document.querySelectorAll("feDisplacementMap");
primitives.forEach((primitive) => {
  primitive.setAttribute("scale", 30);
});