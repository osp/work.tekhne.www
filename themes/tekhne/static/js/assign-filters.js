
// everything with this class is going to get a different random filter apply on each letter
let logo_class = '.logo';

function assign_filters(){

    let logos = document.querySelectorAll(logo_class);

    logos.forEach( (logo) => {

        let text = logo.textContent;
        let letters = text.trim().split("");
        logo.innerHTML = '';

        let FILTERS = JSON.parse(JSON.stringify(jsFILTERS));

        
        let i = 0, length = letters.length;
        for (i; i < length; i++) {

            let cond = true;
            if (logo.classList.contains('rand')){
                cond = Math.random() < 0.4;
            }
            if (logo.classList.contains('rand') && letters[i] == ' '){
                cond = false;
            }

            if (cond){
                let css = "";
                if (letters[i] == ' '){
                    css = "class='break'";
                    letters[i] = '&nbsp;';
                }
                else{
                    // pick random filter
                    let random = Math.floor(Math.random() * FILTERS.length);
                    let pick = FILTERS[random];
                    FILTERS.splice(random, 1);
        
                    css = "style='filter: url(#" + pick.id + "')";
                }

                logo.innerHTML += "<span " + css + ">" + letters[i] + "</span>";
            }
            else{
                logo.innerHTML += letters[i];
            }

        }

        logo.classList.add('computed');

    });

}

assign_filters();


let feDisplacementMaps = document.querySelectorAll("#InkscapeFilters feDisplacementMap");
let filters = document.querySelectorAll("#InkscapeFilters filter");

feDisplacementMaps.forEach((feDisplacementMap) => {
    feDisplacementMap.setAttribute("scale", 50);
});
filters.forEach((filter) => {
    // filter.setAttribute("width", "50");
    // filter.setAttribute("height", "50");
    // filter.setAttribute("primitiveUnits", "objectBoundingBox");
    filter.setAttribute("filterRes", "1px");
});