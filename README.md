# work.tekhne.www

TODO if time:

* better filtering js and unfolding show more
* better tag listing and artist listing
* journal view is broken on half screen
* better about page

## How to setup for content only editing

1. Install **Nextcloud client**, on Ubuntu you can

        sudo apt install nextcloud-desktop

2. Create a folder that is going to contain the content of the website, name it how you want to.
3. Using the **Nextcloud client**, set up a synchronization between the `/website/www` folder on <https://cloud.tekhne.website> and the folder you created on your computer.

Now any modifications you will do in this *cloud-synched folder* will spread to the others, and reciprocally. The updates are detected and applied automatically by the **Nextcloud client**.

## How to set up for dev editing

### Install Pelican

1. install **pip** `sudo apt install python3-pip`
2. then use **pip** to install **pelican** `python3 -m pip install pelican[markdown]`

### Point content to the *cloud-synched folder*

1. First, do the same steps as for *content only editing*.
2. Then `git clone` this repo in a folder on your computer.
3. Go into the repo.
 
        cd work.tekhne.wwww

4. Create a **symlink** named `content` that points to the *cloud-synched folder*. Depending on where you did put the *cloud-synched folder* folder relatively, it can look like this:

        ln -s ../cloud.tekhne/www/ content

Now **Pelican** is going to use the *cloud-synched folder*, as it's **content**, to generate the website.

### Generate website locally

To launch Pelican locally and test the website on your computer.

1. Go into your repo `work.tekhne.www` folder and launch a local server with
 
        pelican --listen

2. Generate the website locally 

        pelican

3. Access <http://localhost:8000> in your browser

It should open your local copy of the website.
If you edit anything, either the content in the *cloud-synched folder*, or the **Pelican** files in the repo, you have to `pelican` again to re-generate it.
