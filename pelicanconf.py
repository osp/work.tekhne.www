AUTHOR = 'tekhnē'
SITENAME = 'tekhnē'
SITEURL = ''

PATH = 'content'
THEME = 'themes/tekhne'
PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['Bios', 'Calls', 'Blog', 'Events', 'Artists', 'Journal']
STATIC_PATHS = ['images']
SITELOGO = 'images/logos/eu_logo.jpg'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# ARCHITECTURE
# ==============================================================================


USE_FOLDER_AS_CATEGORY = True
PATH_METADATA = "(?P<category>.*)/.*"

DEFAULT_DATE = 'fs'

# generate direct template (that are not equivalent to one file in the content folder)
DIRECT_TEMPLATES = ['index', 'filter-test-page', 'about', 'artists', 'journal']

# for event_date parsing
LOCALE = 'en_US.utf8'

SUMMARY_MAX_LENGTH = 42

# article have their category in url
ARTICLE_URL = ARTICLE_SAVE_AS = '{category}/{slug}.html'

# disable the generation of those default html files
AUTHOR_URL = AUTHOR_SAVE_AS = ''
TAG_URL = TAG_SAVE_AS = ''
CATEGORY_URL = CATEGORY_SAVE_AS = ''


# FILTER
# ==============================================================================

import datetime

def has_cat(articles, cat):
    # return only articles that have a specific category amongst
    # they categories ancestors
    return [article for article in articles if str(cat) in str(article.category)]

def has_tag(articles, tag, article, selected_articles = False):
    if selected_articles:
        articles = selected_articles
    return [a for a in articles if hasattr(a, 'tags') and tag in a.tags and a != article]

def past(event):
    return event.event_date['max'] < datetime.datetime.now()

def past_deadline(call):
    return call.deadline < datetime.datetime.now()

JINJA_FILTERS = {
    'has_cat': has_cat,
    'has_tag': has_tag,
    'past': past,
    'past_deadline': past_deadline
}

# PLUGINS
# ==============================================================================

PLUGIN_PATHS = ['plugins']

PLUGINS = ['svg_filters', 'more_categories', 'event_date', 'links_genres', 'auto_attach', 'custom_tags', 'image_process']

# --- image_process (from: https://github.com/pelican-plugins/image-process)
# note: we can add the class manually in the html template (and not in the markdown)
IMAGE_PROCESS = {
    # reminder on srctset
    # https://www.youtube.com/watch?v=2QYpkrX2N48&ab_channel=KevinPowell
    # our gallery <img> html element varies between
    # 300 and 500 browser pixels width
    # so on 1x devices we give it a 500px image to be sure
    "cover": {
        "type": "responsive-image",
        "srcset": [
            ("1x", ["scale_in 500 500 True"]),
            ("2x", ["scale_in 1000 1000 True"])
        ],
        "default": "1x"
    },
    # note that this class is added through the auto-attach plugin :)
    "markdown": {
        "type": "responsive-image",
        "srcset": [
            ("1x", ["scale_in 500 500 True"]),
            ("2x", ["scale_in 1000 1000 True"])
        ],
        "default": "1x"
    }
}

# MD EXTENSION
# ==============================================================================

import yafg
# https://pypi.org/project/yafg/

# from markdown_figcap import FigCapExtension
# https://github.com/funk1d/markdown-figcap

# from zettlr2pelican import Zettlr2PelicanExtension

MARKDOWN = {
    'extensions': [
        yafg.YafgExtension(stripTitle=False)
    ],
    'extension_configs': {
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
    },
    'output_format': 'html5',
}

# for officially and third party extensions:
# https://python-markdown.github.io/extensions/

