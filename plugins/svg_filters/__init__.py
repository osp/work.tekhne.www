from pelican import signals
import os
import xml.etree.ElementTree as ET

import logging
log = logging.getLogger(__name__)

LOG_PREFIX = "[svg_filter]"

FILTERS_DIRECTORY = 'themes/tekhne/templates/filters/'
FILTERS_PATHS = [
    'inkscape_filters.svg',
    'custom_filters.svg',
]

FAV_FILTERS = [

    # diy filters made by einar
    'custom1',
    'custom2',
    'custom3',
    'custom4',

    # a selection of filter for black and white typography :)
    # 'f005',
    # 'f038',
    # 'f040',
    # 'f046',
    'f060',
    'f077',
    # 'f086',
    # 'f087',
    'f164',
    # 'f093',
    'f006',
    # 'f027',
    # 'f136',
    'f007',
    # 'f011',
    # 'f034',
    # 'f090',
    'f094',
    # 'f151',
    # 'f185',
    # 'f047',
    # 'f048',
    # 'f049',
    # 'f051',
    # 'f066', removed in the end because it makes certain letter dissapear
    # 'f067',
    # 'f068',
    # 'f070',
    # 'f020',
    # 'f033',
    # 'f101',
    'f102',
    # 'f108',
    # 'f138',
    'f144',
    # 'f156',
    # 'f037',
    # 'f043',
    # 'f054',
    'f057',
    # 'f059',
    # 'f188',
    # 'f075',
    'f227',
    'f231',
]

def instance_filters(generator):

    ns = '{http://www.w3.org/2000/svg}'
    nsi = '{http://www.inkscape.org/namespaces/inkscape}'

    filters = []

    for FILTERS_PATH in FILTERS_PATHS:

        filters_file = os.path.join(FILTERS_DIRECTORY, FILTERS_PATH)

        # parse svg filters files
        tree = ET.parse(filters_file)
        root = tree.getroot()

        for filter in root.iter(ns+'filter'):

            # if it's in the favorite filter list
            if filter.attrib['id'] in FAV_FILTERS:
            
                # an inkscape category OR '00-self-made'
                category = 'DIY'
                if nsi+'menu' in filter.attrib:
                    category = filter.attrib[nsi+'menu']

                # write label
                label = filter.attrib['id']
                if nsi+'label' in filter.attrib:
                    label = filter.attrib[nsi+'label']

                filter_object = {
                    'category': category,
                    'label': label,
                    'id': filter.attrib['id'],
                }

                # add category if not already in
                # if category not in filters:
                    # filters[category] = []

                # print('{pre} {l} #{id} ({c})'.format(pre = LOG_PREFIX, l = label, id = filter.attrib['id'], c = category))

                # add filter to list
                filters.append(filter_object)

                # s = []
                # for e in filter:
                #     s.append(ET.tostring(e, encoding="unicode"))
                # filter.attrib['code'] = ''.join(s)
                # filter.attrib['code'] = filter.attrib['code'].replace('ns0:', '')
                # filter.attrib['code'] = filter.attrib['code'].replace('xmlns:ns0="http://www.w3.org/2000/svg"', '')


    # register as a Pelican variable, to easily iterate on it 
    generator.context['SVG_FILTERS'] = filters
    generator.context['SVG_FILTERS_PATHS'] = FILTERS_PATHS


def register():
    signals.article_generator_pretaxonomy.connect(instance_filters)