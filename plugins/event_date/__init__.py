from pelican import signals
import datetime
from dateutil.parser import *
import json
import pytz

import logging
import pprint

import locale

log = logging.getLogger(__name__)

utc=pytz.UTC

def parse_date(date_str, article):
    try:
        date = parse(date_str)
    except ParserError as e:
        log.error("DATE ERROR (bad parsing) in {p}\n-> {err} ".format(p = article.relative_source_path, err = str(e)))
    return date

def parse_deadline( article):
    deadline = article.metadata.get('deadline', None)
    # print('dates for {a} : {d}'.format(a = article.title, d = deadline))

    if deadline:
        article.deadline = parse_date(deadline, article)


def compute_date_ranges(article):

    meta_dates = article.metadata.get('event_date', None)
    # print('dates for {a} : {d}'.format(a = article.title, d = meta_dates))

    if meta_dates:

        # split at ";" between date and time
        splitted_meta_dates = meta_dates.split(';')
        if len(splitted_meta_dates) > 1:
            date_range = {
                'date': splitted_meta_dates[0],
                'time': splitted_meta_dates[1],
            }
        else:
            date_range = {
                'date': splitted_meta_dates[0],
            }

        # split date data
        d_range = date_range['date'].split('>')

        # split time data if there is
        has_time = False
        if 'time' in date_range:
            has_time = True
            time_range = date_range['time'].split('>')

        # put the time range in both of dates
        date_range = [  {'d': date , 't': time_range} 
                        if 'time' in date_range
                        else {'d': date} for date in d_range]

        # parsing both date and time
        for d in date_range:
            to_parse = d['d'] + ' ' + d['t'][0] if 't' in d else d['d']
            d['d'] = parse_date(to_parse, article)
            if 't' in d:
                d['t'] = [parse_date(time, article) for time in d['t']]

        # thus we have something like this:
        # date_range = [{
        #   d: d0+t0,
        #   t: [t0, t1]
        # },{
        #   d: d1+t0,
        #   t: [t0, t1]
        # }]

        if len(date_range) == 0:
            log.error("DATE ERROR (no valid date) in {p}".format(p = article.relative_source_path))
        
        parsed_event_date = {
            'full': date_range,
            'min': date_range[0]['d'],
            'max': date_range[-1]['d'],
            'has_time': has_time,
        }

        # save the parsed version
        article.event_date = parsed_event_date
        article.date = article.event_date['min'].replace(tzinfo=utc)
        
        # print("min date: {d0}, max date: {d1}".format(d0 =  article.event_date['min'], d1 = article.event_date['max']))

    else:
        log.error("NO DATE in {p}".format(p = article.relative_source_path))

def event_date(generator):
    # this is the articles list, meaning it only contains
    for article in generator.articles:
        if "Events" in article.category.name:
            compute_date_ranges(article)

def deadline_date(generator):
    for article in generator.articles:
        if "Calls" in article.category.name:
            parse_deadline(article)

def register():
    # signals.article_generator_write_article.connect(date_ranges)

    # we have to parse the date of every article before generator a single one
    # because of the sub_events system
    signals.article_generator_pretaxonomy.connect(event_date)
    signals.article_generator_pretaxonomy.connect(deadline_date)
    
