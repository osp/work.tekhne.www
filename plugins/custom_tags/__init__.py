# -*- coding: utf8 -*-

from collections import defaultdict

from pelican import signals
from pelican.urlwrappers import URLWrapper, Author
from pelican.readers import METADATA_PROCESSORS, ensure_metadata_list

import logging
import os.path

# to behave like tags
metadata_name = 'artists'

_DISCARD = object()

# create a new class from URLWrapper for slugify, etc
class Artists(URLWrapper):
    pass

# Add a new metadata processor
METADATA_PROCESSORS['artists'] = lambda x, y: ([Artists(custom, y) for custom in ensure_metadata_list(x)] or _DISCARD)

def get_custom(article_generator, metadata):
    pass

def create_custom(generator):

    # creating the global variable with listed articles
    generator.artists = []
    custom_dct = defaultdict(list)
    for article in generator.articles:
        if hasattr(article, 'artists'):
            # if artists non-empty:
            for custom in {custom for custom in article.artists}:
                custom_dct[custom].append(article)

    # sorting it
    generator.artists = sorted(
        list(custom_dct.items()),
        reverse = False,
    )

    generator._update_context(['artists'])

def register():
    signals.article_generator_context.connect(get_custom)
    signals.article_generator_finalized.connect(create_custom)
